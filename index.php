<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <title>Главная страница</title>
</head>

<body>

    <header class="header">
        <div class="title">
            <h1>подслушано 8А</h1>
        </div>
        <div class='registration_and_logins'>
            <a href="sign_up.php"><button class='register'>регестрация</button></a>
            <a href="login.php"><button class='login'>вход</button></a>
        </div>
    </header>
    
    <div class="btn_history">
        <button>
            <a class="btn_histori" href="add_story.php">написать <img class="plus" src="images/plus.png" alt=""> историю</a>
        </button>
    </div>

    <h1><?= $type ?></h1>
    <section class="category_and_post">
        <?php require "category.php"; ?>
        <section class="box_with_posts">
            <?php
                require "actions/delete_post.php";
                require "actions/output_posts.php";
            ?>
        </section>
    </section>

</body>
</html>