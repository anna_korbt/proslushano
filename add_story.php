<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <title>Add Story</title>
</head>

<body>

    <?php
        require "actions/checking_existing_history.php";
    ?>
    <form class="form_post_adding" action="" method="POST">
        <input class="inputs_second" name="name" type="text" placeholder="название" >
        <textarea class="textarea" name="text" placeholder="текст"></textarea>

        <select name="sample_select">
            <?php require "actions/output_story_categories.php";?>
        </select>

        <select class="sample_select_second" name="type">
            <?php require "actions/output_option_category.php";?>
        </select>

        <button name="btn" class="send_dtn">oтправить</button>

        <h5 style="color: <?=$color?>;"><?=$message?></h5>
    </form>

    <h1><a class="back_main_page" href="index.php">вернуться на главную страницу</a></h1>

</body>
</html>