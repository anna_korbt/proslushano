<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles/style.css">

    <title>Login</title>
</head>
<body>

    <?php
        require "actions/add_login.php";
    ?>
    <form class="login_to_sait" method="POST" action="">
        
        <label class="input">
            <div>
                <img src="images/user.png" alt="">
            </div>
            <input  class="username_and_password" type="text"name="login" placeholder="Username" >
        </label>

        <label class='input'>
            <div>
                <img src="images/key.png" alt="">
            </div>
            <input type="text"name="password" class="username_and_password" placeholder="Password">
        </label>

        <button class="login_btn" type="submit" name="button">Войти</button>
        <p class='not_all_parameters'><?= $error_message;?></p>
        
    </form>
    <a class="fogot" href=""><p>Fogot password?</p></a>

</body>
</html>
