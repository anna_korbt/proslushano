<?php
    $db=mysqli_connect ("localhost","root","","podslushano");
    $category_table = mysqli_query ( $db, "SELECT * FROM categories " );
    $categories=[];    
    while( $category_table != NULL ){        
        if( ($row = $category_table->fetch_assoc()) == NULL ){
            break;        
        }
        else{
            array_push ($categories,$row['type']);
        }
    }
    $query="SELECT * FROM `post` ";
    $isUser = false;
    $isOperator = false;
    foreach( $categories as $category )
    {
        if( isset( $_GET[ $category ] ) )
        {
            if($isUser==false && $isOperator==false){
                $query .= " WHERE ( "; 
            }  
            if( $isUser ){
                $query .= " || ";
            }
            
            $query .= "category='$category'";
            $isUser = true;
        }
    }
    if( $isUser ){
        $query .= ')';
    }
    $useres_table = mysqli_query ( $db, "SELECT * FROM user " );
    $users=[];
    while( $useres_table != NULL ){        
        if( ($row = $useres_table->fetch_assoc()) == NULL ){
            break;        
        }
        array_push ($users,$row['classmates']); 
    }
    $isOneUser = false;
    foreach( $users as $user )
    {
        if( isset( $_GET[ str_replace (" ", "_", $user) ] ) )
        {
            if( !$isOneUser ){
                $query.= " && (";
                $isOperator = false;
                $isOneUser = true;
            }
            else if( $isUser ){
                $query .= " || ";
            }
            if($isUser==false && $isOperator==false){
                $query .= " WHERE "; 
            }  
            $user_name = $user;
            $query .= "user_name='$user_name'";
            $isUser = true;
            $isOperator = true;
        }
    }
    if( $isOneUser ){
        $query.= ")";
    }

    $isUser = false;
    mysqli_close ($db);
?>