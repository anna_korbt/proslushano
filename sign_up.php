<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles/style.css">

    <title>Sign up</title>
</head>

<?php
    require "actions/add_sign_up.php";
?>

<body>
    <form class="login_to_sait" method="POST" action="">
        <label class="input">
            <div>
                <img src="images/user.png" alt="">
            </div>
            <input type="text"name="login" placeholder="Username">
        </label>

        <label class="input">
            <div>
                <img src="images/key.png" alt="">
            </div>
            <input type="password"name="password" placeholder="Password">
        </label>

        <label class="input">
            <div>
                <img src="images/email.png" alt="">
            </div>
            <input type="text" class="emeils" name="email" placeholder="email">
        </label>
        
        <button class="login_btn" type="submit" name="button">Зарегистрироваться</button>
        <p class='not_all_parameters'><?= $error_messages;?></p>
    </form>

</body>
</html>
